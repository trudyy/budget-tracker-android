package com.example.trudy.track_budget_android.ui.graph;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.trudy.track_budget_android.R;
import com.example.trudy.track_budget_android.models.Expense;
import com.example.trudy.track_budget_android.models.Week;
import com.example.trudy.track_budget_android.ui.MainActivity;
import com.example.trudy.track_budget_android.utils.ExpenseUtils;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class GraphFragment extends Fragment {

    private View rootView;

    private int weekInYear = 1;
    private ArrayList<Expense> mListExpenses = new ArrayList<>();
    private double[] mTotal = new double[7];
    private ArrayList<String> daysOfWeek = new ArrayList<>(Arrays.asList("Sun", "Mon", "Tues", "Wed", "Thu", "Fri", "Sat"));

    public GraphFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GraphFragment.
     */
    public static GraphFragment newInstance(Bundle bundle) {
        GraphFragment fragment = new GraphFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Passed by MainSlidePagerAdapter
        if (getArguments() != null) {
            weekInYear = getArguments().getInt(MainActivity.ARG_WEEKINYEAR);
            mListExpenses = getArguments().getParcelableArrayList(MainActivity.ARG_EXPENSES);
            mTotal = getArguments().getDoubleArray(MainActivity.ARG_TOTALPERDAY);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_graph, container, false);

        // Render the graph depending on which week in the year
        renderBarGraph();
        updateEmptyView();

        return rootView;
    }

    /**
     * Renders the bar graph for a specified week in the year
     */
    private void renderBarGraph() {

        if (mListExpenses.size() > 0 && (mTotal.length == 7)) {

            // Create the dataset
            ArrayList<BarEntry> entries = new ArrayList<>();
            for (int i=0; i<mTotal.length; i++) {
                entries.add(new BarEntry(new Float(mTotal[i]), i));
            }

            BarDataSet barDataSet = new BarDataSet(entries, "Amount spent each day of the week");
            BarData barData = new BarData(daysOfWeek, barDataSet);

            // Create the chart
            BarChart chart = (BarChart) rootView.findViewById(R.id.chart);
            chart.setData(barData);
        }
    }

    private void updateEmptyView() {
        LinearLayout emptyView = (LinearLayout) rootView.findViewById(R.id.empty_chart);
        BarChart chart = (BarChart) rootView.findViewById(R.id.chart);

        if (mListExpenses.size() > 0) {
            emptyView.setVisibility(View.GONE);
            chart.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
            chart.setVisibility(View.INVISIBLE);
        }
    }
}
