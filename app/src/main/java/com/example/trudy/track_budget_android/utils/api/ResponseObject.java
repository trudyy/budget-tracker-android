package com.example.trudy.track_budget_android.utils.api;

import android.util.Log;

import com.example.trudy.track_budget_android.models.Expense;
import com.example.trudy.track_budget_android.models.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.StringReader;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by trudy on 2017-07-21.
 */

public class ResponseObject {

    @SerializedName("error") @Expose public boolean error;
    @SerializedName("message") @Expose public String message;
    @SerializedName("data") @Expose public Object data;

    // 2001-07-04T12:08:23PM
    private static String dateFormat = "yyyy-MM-dd'T'hhmmssaaa";
    private static String TAG = "ResponseObject";
    private Gson gson = new GsonBuilder()
            .registerTypeHierarchyAdapter(Expense.class, new ExpenseDeserializer())
            .setPrettyPrinting()
            .disableHtmlEscaping()
            .create();

    public User getUserResponse() {
        Log.e(TAG, "getUserResponse");
        Log.e(TAG, this.data.toString());

        try {
            User user = gson.fromJson(this.data.toString(), User.class);
            return user;
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return new User();
        }
    }


    public ArrayList<Expense> getExpenseListResponse() {
        Log.e(TAG, "getExpenseListResponse");

        String myString = this.data.toString().replaceAll(" ", "%20").replaceAll(",%20", ", ");
        Type listType = new TypeToken<ArrayList<Expense>>(){}.getType();

        JsonReader reader = new JsonReader(new StringReader(myString));
        reader.setLenient(true);

        try {
            ArrayList<Expense> expenses = gson.fromJson(reader, listType);
            return expenses;
        } catch (JsonSyntaxException e) {
            Log.e(TAG, "json syntax exception");
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

//    /**
//     * Handle Dates - Serialize
//     * Reference: https://stackoverflow.com/questions/28540224/retrofit-gson-serialize-date-from-json-string-into-long-or-java-lang-long
//     */
//    static class DateSerializer implements JsonSerializer<Date> {
//
////        private final String TAG = DateSerializer.class.getSimpleName();
//
//        @Override
//        public JsonElement serialize(Date date, Type type, JsonSerializationContext jsonSerializationContext) {
//
//            Log.e(TAG, "Serialized Format: "+date.toString());
//            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
//            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
//            String dateFormatAsString = formatter.format(date);
//            Log.e(TAG, "Serialized Date: "+dateFormatAsString);
//
//            return new JsonPrimitive(dateFormatAsString);
//        }
//    }
//
//    /**
//     * Handle Dates - Deserialize
//     * Reference: https://stackoverflow.com/questions/28540224/retrofit-gson-serialize-date-from-json-string-into-long-or-java-lang-long
//     */
//    static class DateDeserializer implements JsonDeserializer<Date> {
//
//        @Override
//        public Date deserialize(JsonElement element, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
//
//            String date = element.getAsString();
//            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//            Log.e(TAG, "Date Format: " + formatter.toString());
//            formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
//            Date returnDate = null;
//            try {
//                returnDate = formatter.parse(date);
//            } catch (ParseException e) {
//                Log.e(TAG, "Date parser exception:", e);
//                returnDate = null;
//            }
//            return returnDate;
//        }
//    }

    /**
     * Handle Expenses - Deserialize
     * Reference: https://stackoverflow.com/questions/28540224/retrofit-gson-serialize-date-from-json-string-into-long-or-java-lang-long
     */
    static class ExpenseDeserializer implements JsonDeserializer<Expense> {

        @Override
        public Expense deserialize(JsonElement element, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {

            Expense expense = new Expense();
            try {
                Log.e(TAG, "ExpenseDeserializer");
                JsonObject expenseObj = element.getAsJsonObject();
                Log.e(TAG, expenseObj.toString());
                expense.id = expenseObj.get("id").getAsInt();
                expense.user = expenseObj.get("user").getAsInt();
                expense.description = expenseObj.get("description").getAsString().replaceAll("%20", " ");
                expense.amount = expenseObj.get("amount").getAsDouble();

                SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
                formatter.setTimeZone(TimeZone.getTimeZone("GMT"));

                // First try date
                try {
                    String date = expenseObj.get("date").getAsString();
                    expense.date = formatter.parse(date);

                }  catch (ParseException e) {
                    Log.e(TAG, "Date parser exception");
                    e.printStackTrace();
                    expense.date = null;
                }
                // Try createdAt
                try {
                    String createdAt = expenseObj.get("createdAt").getAsString();
                    expense.createdAt = formatter.parse(createdAt);

                } catch (ParseException e2) {
                    Log.e(TAG, "Date parser exception - createdAt");
                    e2.printStackTrace();
                    expense.createdAt = null;
                }
                // Try updatedAt
                try {
                    String updatedAt = expenseObj.get("updatedAt").getAsString();
                    expense.updatedAt = formatter.parse(updatedAt);

                } catch (ParseException e3) {
                    Log.e(TAG, "Date parser exception - updatedAt");
                    e3.printStackTrace();
                    expense.updatedAt = null;
                }

            } catch (JsonSyntaxException je) {
                Log.e(TAG, "Json syntax exception");
                je.printStackTrace();

            }

            return expense;
        }
    }
}
