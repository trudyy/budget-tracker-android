package com.example.trudy.track_budget_android.utils.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.trudy.track_budget_android.models.Expense;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by trudy on 2017-07-08.
 * Code based on: Beginning Android Programming with Android Studio J.F. Dimarzio
 */
public class DBAdapter {

    static final String KEY_ROWID           = "_id";
    static final String KEY_DESCRIPTION     = "description";
    static final String KEY_AMOUNT          = "amount";
    static final String KEY_DATE            = "date";
    static final String KEY_USERID          = "user";
    static final String KEY_CREATEDAT       = "createdAt";
    static final String KEY_UPDATEDAT       = "updatedAt";
    static final String TAG                 = "DBAdapter";
    static final String DATABASE_NAME       = "mydb";
    static final String DATABASE_TABLE      = "expense";
    private static final int DATABASE_VERSION = 1;

    private static final String EXPENSE_TABLE_DELETE = "DROP TABLE IF EXISTS " + DATABASE_TABLE;
    private static final String EXPENSE_TABLE_CREATE = "CREATE TABLE " + DATABASE_TABLE + " (" +
            KEY_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            KEY_DESCRIPTION + " TEXT NOT NULL, " +
            KEY_AMOUNT + " TEXT NOT NULL, " +
            KEY_USERID + " INTEGER NOT NULL, " +
            KEY_DATE + " INTEGER NOT NULL," +
            KEY_CREATEDAT + " INTEGER NOT NULL," +
            KEY_UPDATEDAT + " INTEGER NOT NULL" +
    ");";

    final Context context;
    DBHelper eHelper;
    SQLiteDatabase db;

    /**
     * Constructor
     * @param context
     */
    public DBAdapter(Context context) {
        this.context = context;
        eHelper = new DBHelper(context);
    }

    /**
     * SQLiteOpenHelper
     */
    public class DBHelper extends SQLiteOpenHelper {

        DBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.e(TAG, "Creating database");
            db.execSQL(EXPENSE_TABLE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.e(TAG, "Upgrading database from version "
                    + oldVersion + " to " + newVersion + ", which will destroy all old data");
            db.execSQL(EXPENSE_TABLE_DELETE);
            onCreate(db);
        }
    }

    /**
     * Opens the database
     * Note: "Since getWritableDatabase() and getReadableDatabase() are expensive to call when the database is closed,
     * you should leave your database connection open for as long as you possibly need to access it"
     * @return
     * @throws SQLException
     */
    public DBAdapter open() throws SQLException {
        Log.e(TAG, "Opening database");
        db = eHelper.getWritableDatabase();
        return this;
    }

    /**
     * Closes the database
     * Note: "Typically it is optimal to close the database in the onDestroy()"
     */
    public void close() {
        Log.e(TAG, "Closing database");
        eHelper.close();
    }

    /**
     * Inserts an expense
     * @param description
     * @param amount
     * @param date
     * @return -1 = UNIQUE constraint failed
     */
    public long insertExpense(int id, String description, Double amount, Date date, int user, Date createdAt, Date updatedAt) {
        ContentValues values = new ContentValues();
        if (id != 0) { values.put(KEY_ROWID, id); }
        values.put(KEY_DESCRIPTION, description);
        values.put(KEY_AMOUNT, amount);
        values.put(KEY_DATE, date.getTime());
        values.put(KEY_USERID, user);
        values.put(KEY_CREATEDAT, createdAt.getTime());
        values.put(KEY_UPDATEDAT, updatedAt.getTime());
        long dbID = db.insert(DATABASE_TABLE, null, values); // returns -1 if unique constraint fails
        Log.e(TAG, "insertExpense => id: "+dbID);
        Log.e(TAG, "insertExpense => user id: "+user);
        return dbID;
    }
    /**
     * Delete an Expense
     * @param rowId
     * @return
     */
    public boolean deleteExpense(long rowId) {
        Log.e(TAG, "deleteExpense => id: "+rowId);
        return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
    }

    /**
     * Get all Expenses
     * @return
     */
    public ArrayList<Expense> getAllExpenses(int userId) {
        Log.e(TAG, "getAllExpenses for user: " + userId);

        ArrayList<Expense> mResult = new ArrayList<>();

        // Retrieve the raw cursor data from the database
        Cursor mCursor = db.query(DATABASE_TABLE,
                new String[] { KEY_ROWID, KEY_DESCRIPTION, KEY_AMOUNT, KEY_DATE, KEY_USERID, KEY_CREATEDAT, KEY_UPDATEDAT },
                KEY_USERID + "=" + userId,
                null, null, null, null);

        // Build the arraylist of expenses from the raw data
        if (mCursor.moveToFirst()) {
            do {
                String id           = mCursor.getString(mCursor.getColumnIndex(KEY_ROWID));
                String description  = mCursor.getString(mCursor.getColumnIndex(KEY_DESCRIPTION));
                String amount       = mCursor.getString(mCursor.getColumnIndex(KEY_AMOUNT));
                long date           = mCursor.getLong(mCursor.getColumnIndex(KEY_DATE));
                String user         = mCursor.getString(mCursor.getColumnIndex(KEY_USERID));
                long createdAt      = mCursor.getLong(mCursor.getColumnIndex(KEY_CREATEDAT));
                long updatedAt      = mCursor.getLong(mCursor.getColumnIndex(KEY_UPDATEDAT));
                Log.e(TAG, "getAllExpenses => id: "+id);
                Expense expense     = new Expense(id, description, amount, date, user, createdAt, updatedAt);
//                expense.print();
                mResult.add(expense);
            } while (mCursor.moveToNext());
        }

        Log.e(TAG, "Result has "+mResult.size());

        return mResult;
    }

    /**
     * Get an Expense
     * @param rowId
     * @return
     */
    public Expense getExpense(long rowId) {
        Log.e(TAG, "getExpense");

        // Retrieve the raw cursor data from the database
        Cursor mCursor = db.query(true, DATABASE_TABLE,
                new String[] { KEY_ROWID, KEY_DESCRIPTION, KEY_AMOUNT, KEY_DATE, KEY_USERID },
                KEY_ROWID + "=" + rowId,
                null, null, null, null, null);

        // Parse out the expense
        if ((mCursor != null) && (mCursor.moveToFirst())) {
            String id           = mCursor.getString(mCursor.getColumnIndex(KEY_ROWID));
            String description  = mCursor.getString(mCursor.getColumnIndex(KEY_DESCRIPTION));
            String amount       = mCursor.getString(mCursor.getColumnIndex(KEY_AMOUNT));
            long date           = mCursor.getLong(mCursor.getColumnIndex(KEY_DATE));
            String user         = mCursor.getString(mCursor.getColumnIndex(KEY_USERID));
            long createdAt      = mCursor.getLong(mCursor.getColumnIndex(KEY_CREATEDAT));
            long updatedAt      = mCursor.getLong(mCursor.getColumnIndex(KEY_UPDATEDAT));
            return new Expense(id, description, amount, date, user, createdAt, updatedAt);
        }
        return null;
    }

    /**
     * Update an Expense
     * @param rowId
     * @param description
     * @param amount
     * @param date
     * @return
     */
    public boolean updateExpense(long rowId, String description, Double amount, Date date, int user) {
        Log.e(TAG, "updateExpense => id: "+rowId);

        ContentValues args = new ContentValues();
        args.put(KEY_DESCRIPTION, description);
        args.put(KEY_AMOUNT, amount);
        args.put(KEY_DATE, date.getTime());
        args.put(KEY_USERID, user);
        return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
    }
}


