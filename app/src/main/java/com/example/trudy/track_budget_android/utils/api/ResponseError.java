package com.example.trudy.track_budget_android.utils.api;

/**
 * Created by trudy on 2017-07-21.
 * Code from: https://futurestud.io/tutorials/retrofit-2-simple-error-handling
 */

public class ResponseError {

    private int statusCode;
    private String message;

    public ResponseError() {
    }

    public int status() {
        return statusCode;
    }

    public String message() {
        return message;
    }

}
