package com.example.trudy.track_budget_android.ui;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.trudy.track_budget_android.R;
import com.example.trudy.track_budget_android.interfaces.ExpenseInterface;
import com.example.trudy.track_budget_android.listeners.OnExpenseListInteractionListener;
import com.example.trudy.track_budget_android.models.Expense;
import com.example.trudy.track_budget_android.models.User;
import com.example.trudy.track_budget_android.ui.dashboard.DashboardFragment;
import com.example.trudy.track_budget_android.ui.detail.DetailsFragment;
import com.example.trudy.track_budget_android.ui.detail.ExpenseFragment;
import com.example.trudy.track_budget_android.utils.Session;
import com.example.trudy.track_budget_android.utils.api.ErrorUtils;
import com.example.trudy.track_budget_android.utils.api.ResponseError;
import com.example.trudy.track_budget_android.utils.api.ResponseObject;
import com.example.trudy.track_budget_android.utils.api.RetrofitServiceGenerator;
import com.example.trudy.track_budget_android.utils.sqlite.DBAdapter;

import org.w3c.dom.Text;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements OnExpenseListInteractionListener,
        ExpenseFragment.onDialogInteractionListener,
        NavigationView.OnNavigationItemSelectedListener {

    public static String ARG_WEEKINYEAR = "WEEKINYEAR";
    public static String ARG_EXPENSES = "LISTEXPENSES";
    public static String ARG_TOTALPERDAY = "TOTALPERDAY";
    private String TAG = "MainActivity";
    private int userId = 0;

    // Content
    private DBAdapter db;
    private ArrayList<Expense> mListExpenses;
    private int weekInYear = 1;
    private ProgressDialog progress;
    private FloatingActionButton fabAddExpense;

    // Toolbar
    private TextView thisWeek;
    private ImageButton goBackWeek;
    private ImageButton goForwardWeek;

    // Navigation Drawer
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    // Bottom navigation
    private BottomNavigationView navigation;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_graph:
                    goToGraphs();
                    return true;
                case R.id.navigation_dashboard:
                    goToDashboard();
                    return true;
                case R.id.navigation_notifications:
                    goToDetails();
                    return true;
                default:
                    goToDashboard();
                    return true;
            }
        }

    };

    // Page Slider
    private ViewPager mPager;
    private MainSlidePagerAdapter mPagerAdapter;
    private ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
        @Override
        public void onPageSelected(int position) {
            weekInYear = position;
            updateToggleDates(position);
        }
        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_drawer);

        // If the user is not logged in
        Session session = new Session(this);
        userId = session.getUser().id;
        if (session.getUser().token.equals("")) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
            return;
        }

        // Progress bar
        progress = new ProgressDialog(this);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);

        // Setup toolbar and show the navigation drawer icon
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        thisWeek = (TextView) findViewById(R.id.toolbarWeek);
        goBackWeek = (ImageButton) findViewById(R.id.toolbarGoBack);
        goForwardWeek = (ImageButton) findViewById(R.id.toolbarGoForward);
        setupToolbar();

        // Navigation drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState(); // Need this to show hamburger menu icon

        // Navigation contents
        setupNavigationSidebar();

        // Specific to details fragment
        fabAddExpense = (FloatingActionButton) findViewById(R.id.fabAddExpense);
        fabAddExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddExpenseItem();
            }
        });

        // Bottom navigation
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_dashboard);

        // Get the current week in year based on today's date
        getCurrentWeekInYear();
        // Interactions with api and database for expenses
        db = new DBAdapter(this);
        // Retrieve data to show on the screen!
        try {
            db.open();
            getData();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Also close the SQLite database
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (db != null) {
            db.close();
        }
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }

    /**
     * Get the current week in year based on today's date
     */
    private void getCurrentWeekInYear() {
        final Calendar mCalendar = Calendar.getInstance();
        weekInYear = mCalendar.get(Calendar.WEEK_OF_YEAR);
    }

    /**
     * ========================================================================================
     *  Toolbar functions
     * ========================================================================================
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
//        getMenuInflater().inflate(R.menu.toolbar_spacer, menu);
//        MenuItem spacer = menu.findItem(R.id.toolbarSpacer);
//        spacer.setEnabled(false);
//        spacer.setVisible(false);

        return true;
    }

    public void setupToolbar() {
        // Setup date picker
        final Calendar mCalendar = Calendar.getInstance();
        mCalendar.set(Calendar.WEEK_OF_YEAR, weekInYear);
        final DatePickerDialog dateDialog = new DatePickerDialog(MainActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mCalendar.set(Calendar.YEAR, year);
                        mCalendar.set(Calendar.MONTH, monthOfYear);
                        mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        weekInYear = mCalendar.get(Calendar.WEEK_OF_YEAR);
                        // Update the toolbar content
                        updateToggleDates(weekInYear);
                        // Update the fragment contents
                        navigation.setSelectedItemId(navigation.getSelectedItemId());
                    }
                },
                mCalendar.get(Calendar.YEAR),
                mCalendar.get(Calendar.MONTH),
                mCalendar.get(Calendar.DAY_OF_MONTH)
        );

        thisWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateDialog.show();
            }
        });

        goBackWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (weekInYear > 1) {
                    weekInYear--;
                    // Update the toolbar content
                    updateToggleDates(weekInYear);
                    // Update the fragment contents
                    navigation.setSelectedItemId(navigation.getSelectedItemId());
                }
            }
        });

        goForwardWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (weekInYear < 52) {
                    weekInYear++;
                    // Update the toolbar content
                    updateToggleDates(weekInYear);
                    // Update the fragment contents
                    navigation.setSelectedItemId(navigation.getSelectedItemId());
                }
            }
        });
    }
    /**
     * Handle the navigation drawer toggle and other items in the toolbar
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return false;
    }

    public String buildDateRange(int weekInYear) {
        // Retrieve the start and end dates of the week
        Calendar mCalendar = Calendar.getInstance();
        mCalendar.set(mCalendar.YEAR, 2017);
        mCalendar.set(mCalendar.WEEK_OF_YEAR, weekInYear);
        mCalendar.set(mCalendar.DAY_OF_WEEK, Calendar.SUNDAY);
        Date start = mCalendar.getTime();
        mCalendar.set(mCalendar.DAY_OF_WEEK, Calendar.SATURDAY);
        Date end = mCalendar.getTime();

        // Format into string
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM dd", Locale.getDefault());
        String startDate = sdf.format(start);
        String endDate = sdf.format(end);
        String dateRange = startDate + " - " + endDate;
        return dateRange;
    }
    /**
     * Checks whether the buttons should be displayed or not
     */
    private void updateToggleDates(int weekInYear) {

        if (thisWeek != null) {
            // Update Week Description
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.WEEK_OF_YEAR, weekInYear);
            thisWeek.setText(buildDateRange(weekInYear));
//            thisWeek.setTitle(new SimpleDateFormat("MMM").format(cal.getTime()) + " #" + cal.get(Calendar.DAY_OF_WEEK_IN_MONTH));
//            thisWeek.setTitle("Week "+ weekInYear);
        }

        if (goForwardWeek != null) {
            // Check Forward Button
            if (weekInYear == 52) {
                goForwardWeek.setEnabled(false);
                goForwardWeek.setVisibility(View.GONE);
            } else {
                goForwardWeek.setEnabled(true);
                goForwardWeek.setVisibility(View.VISIBLE);
            }
        }

        if (goBackWeek != null) {
            // Check Back Button
            if (weekInYear == 1) {
                goBackWeek.setEnabled(false);
                goBackWeek.setVisibility(View.GONE);
            } else {
                goBackWeek.setEnabled(true);
                goBackWeek.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * ========================================================================================
     *  Nav drawer functions - Android functions
     * ========================================================================================
     */

    private void setupNavigationSidebar() {

        // Setup listener
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Get the user from session
        Session session = new Session(this);
        User mUser = session.getUser();

        TextView mName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_info_name);
        mName.setText(mUser.username);

        TextView mEmail = (TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_info_email);
        mEmail.setText(mUser.email);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        // Close the navigation drawer instead of exiting the app
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()) {
//            case R.id.nav_profile:
//                onViewProfile();
//                break;
            case R.id.nav_sync:
                onSyncData();
                break;
            case R.id.nav_clear:
                onClearData();
                break;
            case R.id.nav_logout:
                onLogout();
                break;
        }

        return true;
    }

    private void onViewProfile() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
    }

    /**
     * Clear device's database table of expenses
     */
    private void onClearData() {
        boolean success = true;

        if (mListExpenses.size() < 1) {
            Toast.makeText(MainActivity.this, "Nothing to clear", Toast.LENGTH_LONG).show();
            return;
        }

        if (progress == null) { progress = new ProgressDialog(this); }
        // Setup progress indicator
        progress.setMessage("Deleting Data");
        progress.show();

        Iterator<Expense> mExpIterator = mListExpenses.iterator();
        while (mExpIterator.hasNext()) {
            Expense e = mExpIterator.next();
            boolean result = db.deleteExpense(e.id);
            Log.e(TAG, "onClearData delete status: "+result);
            if (!result) {
                success = false; // If any failure
            }
            mExpIterator.remove();
        }

        if (success) {
            // Update the fragment contents
            navigation.setSelectedItemId(navigation.getSelectedItemId());

            Toast.makeText(MainActivity.this, "Successfully cleared device expenses", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(MainActivity.this, "Error clearing all device expenses", Toast.LENGTH_LONG).show();
        }
        progress.dismiss();
    }

    /**
     * When the user wants to sync data between device and api
     */
    private void onSyncData() {

        // Setup progress indicator
        progress.setMessage("Syncing Data");
        progress.show();

        // Get the user from session
        Session session = new Session(this);
        int userId = session.getUser().id;
        Log.e(TAG, "USER: " + userId);

        // Get the internal database contents
        ArrayList<Expense> mSqliteExpenses = db.getAllExpenses(userId);

        ExpenseInterface mExpenseInterface = RetrofitServiceGenerator.createService(ExpenseInterface.class, session.getUser().token);
        Call<ResponseObject> call = mExpenseInterface.syncData(session.getUser().id, mSqliteExpenses);
        call.enqueue(new Callback<ResponseObject>() {
            @Override
            public void onResponse(Call<ResponseObject> call, Response<ResponseObject> response) {
                if (response.isSuccessful()) {
                    Log.e(TAG, "onResponse - isSuccessful");
                    Log.e(TAG, "Message - "+response.body().message);
                    Log.e(TAG, "Data - "+response.body().data);

                    // Get response as user
                    ArrayList<Expense> mApiExpenses = response.body().getExpenseListResponse();

                    // Add it to the shared variable..
                    for (Expense e: mApiExpenses) {
                        mListExpenses.add(e);
                    }

                    // Update the fragment contents
                    navigation.setSelectedItemId(navigation.getSelectedItemId());

                    // Populate the device's database
                    if (mApiExpenses.size() > 0) {
                        onPutSqliteData(mApiExpenses);
                    } else {
                        Toast.makeText(MainActivity.this, "Nothing new on the api to add", Toast.LENGTH_LONG).show();
                    }
                    if (progress != null) { progress.dismiss(); }

                } else {
                    // Get the response error using ErrorUtils
                    ResponseError error = ErrorUtils.parseError(response);
                    Log.e(TAG, "onResponse fail - " + error.message());
                    Toast.makeText(MainActivity.this, error.message(), Toast.LENGTH_LONG).show();
                    if (progress != null) { progress.dismiss(); }
                }
            }
            @Override
            public void onFailure(Call<ResponseObject> call, Throwable t) {
                Log.e(TAG, "onFailure - " + t.getMessage());
                Log.e(TAG, t.getMessage());
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                if (progress != null) { progress.dismiss(); }
            }
        });
    }

    /**
     * Inserts expenses into the device's database
     * @param mSqliteExpenses
     */
    private void onPutSqliteData(ArrayList<Expense> mSqliteExpenses) {

        boolean success = true;
        for (Expense e : mSqliteExpenses) {
            long newRowId = db.insertExpense(e.id, e.description, e.amount, e.date, e.user, e.createdAt, e.updatedAt);
            Log.e(TAG, "onPutSqliteData insert status: "+newRowId);
            if (newRowId == -1) { success = false; }
        }

        if (success) {
            Toast.makeText(MainActivity.this, "Successfully saved data to internal database", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(MainActivity.this, "Error saving expense to internal database", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Gets the data from the api when the device's database is empty - on login
     */
    private void onGetApiData() {

        // Setup progress indicator
        progress.setMessage("Retrieving Data");
        progress.show();

        Session session = new Session(this);
        Log.e(TAG, "USER: " + session.getUser().id);

        ExpenseInterface mExpenseInterface = RetrofitServiceGenerator.createService(ExpenseInterface.class, session.getUser().token);
        Call<ResponseObject> call = mExpenseInterface.listUsersExpense(session.getUser().id);
        call.enqueue(new Callback<ResponseObject>() {
            @Override
            public void onResponse(Call<ResponseObject> call, Response<ResponseObject> response) {
                if (response.isSuccessful()) {
                    Log.e(TAG, "onResponse - isSuccessful");
                    Log.e(TAG, "Message - "+response.body().message);
                    Log.e(TAG, "Data - "+response.body().data);

                    // Get response as user
                    mListExpenses = response.body().getExpenseListResponse();
                    mPagerAdapter.notifyDataSetChanged();

                    // Update the fragment contents
                    navigation.setSelectedItemId(navigation.getSelectedItemId());

                    // Populate the device's database
                    if (mListExpenses.size() > 0) {
                        onPutSqliteData(mListExpenses);
                    } else {
                        Toast.makeText(MainActivity.this, "Nothing new on the api to add", Toast.LENGTH_LONG).show();
                    }
                    if (progress != null) { progress.dismiss(); }

                } else {
                    // Get the response error using ErrorUtils
                    ResponseError error = ErrorUtils.parseError(response);
                    Log.e(TAG, "onResponse fail - " + error.message());
                    Toast.makeText(MainActivity.this, error.message(), Toast.LENGTH_LONG).show();
                    if (progress != null) { progress.dismiss(); }
                }
            }
            @Override
            public void onFailure(Call<ResponseObject> call, Throwable t) {
                Log.e(TAG, "onFailure - " + t.getMessage());
                Log.e(TAG, t.getMessage());
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                if (progress != null) { progress.dismiss(); }
            }
        });
    }

    private void onLogout() {
        Session session = new Session(this);
        session.logout();

        Toast.makeText(this, "Logging out...", Toast.LENGTH_SHORT).show();

        finish();
    }

    /**
     * ========================================================================================
     *  Bottom Navigation Drawer functions
     * ========================================================================================
     */

    /**
     * Go to the Home Fragment from clicking in the bottom navigation
     */
    private void goToGraphs() {
        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new MainSlidePagerAdapter("graph", getSupportFragmentManager(), mListExpenses);
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(weekInYear);
        mPager.addOnPageChangeListener(mPageChangeListener);
    }

    /**
     * Go to the Dashboard Fragment from clicking in the bottom navigation
     */
    private void goToDashboard() {
        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new MainSlidePagerAdapter("dashboard", getSupportFragmentManager(), mListExpenses);
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(weekInYear);
        mPager.addOnPageChangeListener(mPageChangeListener);
    }

    /**
     * Go to the Details Fragment from clicking in the bottom navigation
     */
    private void goToDetails() {
        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new MainSlidePagerAdapter("details", getSupportFragmentManager(), mListExpenses);
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(weekInYear);
        mPager.addOnPageChangeListener(mPageChangeListener);
    }

    /**
     * ========================================================================================
     *  From Listener on Expense Details
     * ========================================================================================
     */

    /**
     * Reference: https://stackoverflow.com/questions/2115758/how-do-i-display-an-alert-dialog-on-android
     * @param expense
     */
    @Override
    public void onDeleteExpenseItem(final Expense expense) {
        final Context context = MainActivity.this;
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, R.style.myDialog);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle("Delete Expense")
                .setMessage("Are you sure you want to delete this entry?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                       onDelete(expense);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onClickExpenseItem(Expense expense) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        DialogFragment dialogFragment = ExpenseFragment.newInstance();

        // Tell dialog what type to render
        Bundle arguments = new Bundle();
        arguments.putString(ExpenseFragment.ARG_TYPE, "edit");
        arguments.putParcelable(ExpenseFragment.ARG_EXPENSE, expense);

        dialogFragment.setArguments(arguments);
        dialogFragment.show(ft, "dialog");
    }

    @Override
    public void onAddExpenseItem() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        DialogFragment dialogFragment = ExpenseFragment.newInstance();

        // Tell dialog what type to render
        Bundle arguments = new Bundle();
        arguments.putString(ExpenseFragment.ARG_TYPE, "add");
        dialogFragment.setArguments(arguments);
        dialogFragment.show(ft, "dialog");
    }

    /**
     * ========================================================================================
     *  SQLite Database functions
     * ========================================================================================
     */

    /**
     * Either get the data from SQLite on the device or call the api
     */
    private void getData() {

        // First check if the device database has contents
        mListExpenses = db.getAllExpenses(userId);

        if (mListExpenses.size() == 0 ) {
            // Getting the data from some location
            onGetApiData();
        } else {
            // Update the fragment contents
            navigation.setSelectedItemId(navigation.getSelectedItemId());
        }
    }

    /**
     * Delete an expense
     * @param e
     */
    public void onDelete(Expense e){
        boolean result = db.deleteExpense(e.id);
        if (result) {
            // Get all the expenses again
            mListExpenses = db.getAllExpenses(userId);
            // Update the fragment contents
            navigation.setSelectedItemId(navigation.getSelectedItemId());

            Toast.makeText(this, "Deleted Expense", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Failed to delete expense. Please try again.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * When creating a new expense
     * @param e
     */
    public void onSubmit(Expense e){

        Calendar calendar = Calendar.getInstance();
        long newRowId = db.insertExpense(e.id, e.description, e.amount, e.date, e.user, calendar.getTime(), calendar.getTime());
        if (newRowId != 0) {
            // Get all the expenses again
            mListExpenses = db.getAllExpenses(userId);
            // Update the fragment contents
            navigation.setSelectedItemId(navigation.getSelectedItemId());

            Toast.makeText(this, "Submitted Expense", Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(this, "Failed to save expense. Please try again.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * When updating an existing expense
     * @param expense
     */
    public void onUpdate(Expense expense){
        boolean result = db.updateExpense(expense.id, expense.description, expense.amount, expense.date, expense.user);
        if (result) {
            // Get all the expenses again
            mListExpenses = db.getAllExpenses(userId);
            // Update the fragment contents
            navigation.setSelectedItemId(navigation.getSelectedItemId());

            Toast.makeText(this, "Updated Expense", Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(this, "Failed to update. Please try again", Toast.LENGTH_LONG).show();
        }
    }
}
