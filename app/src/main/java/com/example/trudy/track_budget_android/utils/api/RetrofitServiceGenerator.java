package com.example.trudy.track_budget_android.utils.api;

import com.example.trudy.track_budget_android.utils.Networking;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by trudy on 2017-07-21.
 * Reference: https://stackoverflow.com/questions/35666960/service-generator-retrofit
 */

public class RetrofitServiceGenerator {

    // create an instance of gson to be used when building our service
    public static final Gson gson = new GsonBuilder()
//            .registerTypeAdapter(Date.class, new ResponseObject.DateDeserializer())
//            .registerTypeAdapter(Date.class, new ResponseObject.DateSerializer())
            .setPrettyPrinting()
            .disableHtmlEscaping()
            .create();

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static OkHttpClient client = httpClient.build();

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(Networking.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson));

    /**
     * Creates a service for the class
     * @param serviceClass
     * @param token
     * @return
     */
    public static <T> T createService(Class<T> serviceClass, final String token) {

        if (token != null) {
            // Add the authorization header if the token is passed in
            Interceptor interceptor = new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .header("cache-control","no-cache")
                            .header("Authorization", "Bearer " + token)
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            };

            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .build();

            Retrofit retrofit = builder.client(httpClient).build();
            return retrofit.create(serviceClass);
        }
        // No token
        else {
            return getRetrofit().create(serviceClass);
        }
    }

    public static Retrofit getRetrofit() {
        return builder.build();
    }
}
