package com.example.trudy.track_budget_android.ui.dashboard;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.trudy.track_budget_android.R;
import com.example.trudy.track_budget_android.models.Expense;
import com.example.trudy.track_budget_android.ui.MainActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DashboardFragment extends Fragment {

    private View rootView;

    private int weekInYear = 1;
    private ArrayList<Expense> mListExpenses = new ArrayList<>();

    public DashboardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GraphFragment.
     */
    public static DashboardFragment newInstance(Bundle bundle) {
        DashboardFragment fragment = new DashboardFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Passed by MainSlidePagerAdapter
        if (getArguments() != null) {
            weekInYear = getArguments().getInt(MainActivity.ARG_WEEKINYEAR);
            mListExpenses = getArguments().getParcelableArrayList(MainActivity.ARG_EXPENSES);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);

        // Update page contents
        retrieveWeekRange(weekInYear);
        calculateTotalForWeek();
        getInfoForWeek();

        updateEmptyView();

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void retrieveWeekRange(int weekInYear) {

        // Show on the screen
        TextView dateRangeSummary = (TextView) rootView.findViewById(R.id.weekRangeSummary);
        dateRangeSummary.setText(((MainActivity) getActivity()).buildDateRange(weekInYear));
    }

    private void getInfoForWeek() {

        // Number of items bought
        int count = mListExpenses.size();
        TextView amountSummary = (TextView) rootView.findViewById(R.id.numSummary);
        amountSummary.setText(String.format("%d", count));

        if (count > 0) {
            // Expense - most expense
            Expense mostExpensive = mListExpenses.get(0);
            double highest = mostExpensive.amount;
            TextView mostExpensiveDesc = (TextView) rootView.findViewById(R.id.mostExpensiveDesc);
            TextView mostExpensivePrice = (TextView) rootView.findViewById(R.id.mostExpensivePrice);

            // Expense - least expense
            Expense leastExpensive = mListExpenses.get(0);
            double lowest = leastExpensive.amount;
            TextView leastExpensiveDesc = (TextView) rootView.findViewById(R.id.leastExpensiveDesc);
            TextView leastExpensivePrice = (TextView) rootView.findViewById(R.id.leastExpensivePrice);

            for (Expense e : mListExpenses) {
                if (e.amount < lowest) {
                    leastExpensive = e;
                    lowest = e.amount;
                }
                if (e.amount > highest) {
                    mostExpensive = e;
                    highest = e.amount;
                }
            }

            if (mostExpensive != null) {
                mostExpensiveDesc.setText(mostExpensive.description);
                mostExpensivePrice.setText(String.format("$%.2f", mostExpensive.amount));
            }

            if (leastExpensive != null) {
                leastExpensiveDesc.setText(leastExpensive.description);
                leastExpensivePrice.setText(String.format("$%.2f", leastExpensive.amount));
            }
        }
    }

    private void calculateTotalForWeek() {

        double total = 0.0;
        if (mListExpenses.size() > 0) {
            for (Expense e: mListExpenses) {
                total += e.amount;
            }
        }

        TextView amountSummary = (TextView) rootView.findViewById(R.id.amountSummary);
        amountSummary.setText(String.format("$%.2f", total));
    }

    private void updateEmptyView() {
        LinearLayout emptyView = (LinearLayout) rootView.findViewById(R.id.empty_dashboard);
        LinearLayout dashboard = (LinearLayout) rootView.findViewById(R.id.expense_dashboard);

        if (mListExpenses.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            dashboard.setVisibility(View.INVISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
            dashboard.setVisibility(View.VISIBLE);
        }
    }
}
