package com.example.trudy.track_budget_android.interfaces;

import com.example.trudy.track_budget_android.models.Expense;
import com.example.trudy.track_budget_android.utils.api.ResponseObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by trudy on 2017-07-21.
 * TODO:
 *  - api route conventions
 *  - retrofit HTTP types
 */

public interface ExpenseInterface {

    @POST("expense/create")
    Call<Expense> createExpense(@Body Expense expense);

    @GET("expense/{id}")
    Call<Expense> findExpense(@Path("id") int id);

    @FormUrlEncoded
    @POST("expense/listAllByUser")
    Call<ResponseObject> listUsersExpense(@Field("user") int id);

    @POST("expense/syncDataForUser/{user}")
    Call<ResponseObject> syncData(@Path("user") int userId, @Body ArrayList<Expense> listExpenses);

    @FormUrlEncoded
    @POST("expense/update")
    Call<Expense> updateExpense(@Field("id") int id,
                              @Field("description") String description,
                              @Field("date") String date,
                              @Field("amount") String amount);

    @DELETE("expense/{id}")
    Call<ResponseBody> deleteExpense(@Path("id") int id);

}
