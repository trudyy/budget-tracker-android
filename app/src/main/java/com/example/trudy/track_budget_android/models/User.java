package com.example.trudy.track_budget_android.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by trudy on 2017-07-22.
 */

public class User {

    @SerializedName("id") public int id;
    @SerializedName("username") public String username;
    @SerializedName("email") public String email;
    public String token;

    public User() {
        this.id         = 0;
        this.username   = "";
        this.email      = "";
    }

    // For creating user
    public User(String username, String email) {
        this.username   = username;
        this.email      = email;
    }

    // For retrieving user from api
    public User(int id, String username, String email) {
        this.id         = id;
        this.username   = username;
        this.email      = email;
    }

    @Override
    public String toString() {
        return "User #"+this.id+" ("+this.username+") :"+this.email;
    }
}
