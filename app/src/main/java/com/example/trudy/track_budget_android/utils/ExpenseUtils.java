package com.example.trudy.track_budget_android.utils;

import com.example.trudy.track_budget_android.models.Expense;
import com.example.trudy.track_budget_android.models.Week;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by trudy on 2017-07-02.
 */

public class ExpenseUtils {


    /**
     * Show printouts - for debugging
     * @param list
     */
    public void printouts(ArrayList<Expense> list) {
        for (Expense e: list) {
            e.print();
        }
    }

    /**
     * For creating formatted data to pass to BarGraph
     * @param list
     */
    public HashMap<Integer, Week> groupExpenseByWeek(ArrayList<Expense> list) {

        if ((list==null)||(list.size()==0)) {
            return null;
        }

        // Sort the expenses by dates
        Collections.sort(list, new MyDateComparator());

        // Resulting list - array of groups/weeks
        HashMap<Integer, Week> orderedIntoWeeks = new HashMap<>();

        // Starting group/week
        Week thisWeek = new Week();
        thisWeek.id = list.get(0).getWeekInYearInt();
        thisWeek.addToDayInWeek(list.get(0));

        // Group the expenses by week
        int currWeekInYear = list.get(0).getWeekInYearInt();
        for (int i=1; i < list.size(); i++) {

            // For comparison
            int prevWeekInYear = list.get(i-1).getWeekInYearInt();
            currWeekInYear = list.get(i).getWeekInYearInt();

            // If the current day is part of a different week
            if ((prevWeekInYear != currWeekInYear) && (!thisWeek.isEmpty())) {
                orderedIntoWeeks.put(prevWeekInYear, thisWeek);
                thisWeek = new Week();
                thisWeek.id = currWeekInYear;
                thisWeek.addToDayInWeek(list.get(i));
            } else {
                thisWeek.addToDayInWeek(list.get(i));
            }

        }
        // Add remaining to the final list
        orderedIntoWeeks.put(currWeekInYear, thisWeek);

        return orderedIntoWeeks;
    }

    /**
     * For comparing dates
     * Reference: https://stackoverflow.com/questions/14475556/how-to-sort-arraylist-of-objects
     */
    public class MyDateComparator implements Comparator<Expense> {
        @Override
        public int compare(Expense o1, Expense o2) {
            if (o1.date.after(o2.date)) {
                return 1;
            }
            else if (o1.date.before(o2.date)) {
                return -1;
            }
            return 0; //todo
        }
    }
}
