package com.example.trudy.track_budget_android.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by trudy on 2017-06-05.
 */

public class Expense implements Parcelable {

    @SerializedName("id") @Expose public int id;
    @SerializedName("amount") @Expose public double amount;
    @SerializedName("description") @Expose public String description;
    @SerializedName("date") @Expose public Date date;
    @SerializedName("user") @Expose public int user;
    @SerializedName("createdAt") @Expose public Date createdAt;
    @SerializedName("updatedAt") @Expose public Date updatedAt;

    public Expense() {}

    // For creating expense to sqlite database
    public Expense(String description, Double amount, Date date, int user) {
        setId(0);
        setDescription(description);
        setAmount(amount);
        setDate(date);
        setUser(user);
    }

    // For converting results from the database
    public Expense(String id, String description, String amount, long date,
                   String user, long createdAt, long updatedAt) {
        setId(id);
        setDescription(description);
        setAmount(amount);
        setDate(date);
        setUser(user);
        setCreatedAtDate(createdAt);
        setUpdatedAtDate(updatedAt);
    }

    // Create object with no conversions
    public Expense(int id, String description, double amount, Date date, int user) {
        this.id = id;
        this.amount = amount;
        this.description = description;
        this.date = date;
        this.user = user;
    }

    /**
     * Basic set for each field
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }
    public void setId(String id) {
        // Id
        this.id = -1;
        try {
            this.id = Integer.parseInt(id);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    public void setUser(int user) {
        this.user = user;
    }
    public void setUser(String user) {
        // User
        this.user = -1;
        try {
            this.user = Integer.parseInt(user);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    public void setDescription(String description) {
        // Description
        this.description = description;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    public void setAmount(String amount) {
        // Amount
        this.amount = 0.0;
        try {
            this.amount = new Double(amount);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
    // Date
    public void setDate(Date date) {
        this.date = date;
    }

    public void setCreatedAtDate(long date) {
        this.createdAt = Calendar.getInstance().getTime();
        this.createdAt.setTime(date);
    }
    public void setUpdatedAtDate(long date) {
        this.updatedAt = Calendar.getInstance().getTime();
        this.updatedAt.setTime(date);
    }
    public void setDate(long date) {
        this.date = Calendar.getInstance().getTime();
        this.date.setTime(date);
    }

    /**
     * Get different parts of the date
     * @return
     */
    public int getWeekInYearInt() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.WEEK_OF_YEAR);
    }

    public int getDayInMonthInt() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public int getDayInWeekInt() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK) - 1;
    }

    public String getWeekDayString() {
        String[] weekString = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return weekString[cal.get(Calendar.DAY_OF_WEEK)-1];
    }

    public String getYearString() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return String.valueOf(cal.get(Calendar.YEAR));
    }

    public int getMonthInt() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH);
    }

    public String getMonthDayString() {
        String[] monthString = {"January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return monthString[cal.get(Calendar.MONTH)] + " " + String.valueOf(cal.get(Calendar.DATE));
    }

    public String getFullDateString() {
        if (this.date != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            String result = dateFormat.format(this.date);
            return result;
        } else {
            return "N/A";
        }
    }

    public void setDate(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        try {
            this.date = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public boolean isSameAs(Expense e) {

        Log.e("Comparing", "....");
        e.printObj();
        printObj();

        if (!description.equals(e.description)) {
            return false;
        }
        if (amount != e.amount) {
            return false;
        }
        if (!date.toString().equals(e.description.toString())) {
            return false;
        }
        return true;
    }

    public void printObj() {
        Log.e("Expense #" + this.id,
                "Description: "+this.description +
                "Amount: "+this.amount +
                "Date: "+this.getFullDateString()
        );
    }

    public void print() {
        Log.e("Expense #" + this.id,
                this.getFullDateString() +
                        "\tWinY: " + this.getWeekInYearInt() +
                        "\tDinW: " + this.getDayInWeekInt() +
                        "\tDinM: " + this.getDayInMonthInt() +
                        "\t" + this.description
        );
    }


    /**
     * For inheriting Parcelable
     * @return
     */
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeDouble(amount);
        out.writeString(description);
        out.writeString(getFullDateString());
        out.writeInt(user);
    }

    public static final Parcelable.Creator<Expense> CREATOR = new Parcelable.Creator<Expense>() {
        public Expense createFromParcel(Parcel in) {
            return new Expense(in);
        }

        public Expense[] newArray(int size) {
            return new Expense[size];
        }
    };

    private Expense(Parcel in) {
        id = in.readInt();
        amount = in.readDouble();
        description = in.readString();
        setDate(in.readString());
        user = in.readInt();
    }

}
