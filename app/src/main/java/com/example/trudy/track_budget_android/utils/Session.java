package com.example.trudy.track_budget_android.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.trudy.track_budget_android.models.User;

/**
 * Created by trudy on 2017-06-05.
 */

public class Session {

    private SharedPreferences mSharedPreferences;

    public Session(Context context) {
        mSharedPreferences = context.getSharedPreferences("MySession", Context.MODE_PRIVATE);
    }

    public void setUser(User user) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt("id", user.id);
        editor.putString("username", user.username);
        editor.putString("email", user.email);
        editor.putString("token", user.token);
        editor.commit();
    }

    public User getUser() {
        User user       = new User();
        user.id         = mSharedPreferences.getInt("id", 0);
        user.username   = mSharedPreferences.getString("username", "");
        user.email      = mSharedPreferences.getString("email", "");
        user.token      = mSharedPreferences.getString("token", "");
        return user;
    }

    public void logout() {
        mSharedPreferences.edit().clear().commit();
    }
}
