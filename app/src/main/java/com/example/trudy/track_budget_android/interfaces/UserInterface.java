package com.example.trudy.track_budget_android.interfaces;

import com.example.trudy.track_budget_android.models.User;
import com.example.trudy.track_budget_android.utils.api.ResponseObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by trudy on 2017-07-22.
 */

public interface UserInterface {

    @FormUrlEncoded
    @POST("auth/login")
    Call<ResponseObject> login(@Field("email") String email,
                               @Field("password") String password);

    @FormUrlEncoded
    @POST("auth/register")
    Call<ResponseObject> register(@Field("email") String email,
                                  @Field("password") String password,
                                  @Field("confirm") String confirm);

    @POST("auth/logout")
    Call<ResponseBody> logout(@Field("id") int id);
}
