package com.example.trudy.track_budget_android.ui.detail;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.trudy.track_budget_android.R;
import com.example.trudy.track_budget_android.models.Expense;
import com.example.trudy.track_budget_android.utils.Session;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ExpenseFragment.onDialogInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ExpenseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExpenseFragment extends DialogFragment {

    public static final String ARG_TYPE = "TYPE";
    public static final String ARG_EXPENSE = "EXPENSE";
    public static final String ARG_DATE = "DATE";
    public static final int SHOW_DATE = 111;
    private int userId = 0;

    private View rootView;
    private EditText expenseDetailDesc;
    private EditText expenseDetailAmount;
    private EditText expenseDetailDate;
    private AppCompatButton expenseDetailSubmit;
    private AppCompatButton expenseDetailCancel;

    // Passed from parent fragment
    private String mType;
    private Expense mExpense;

    // Values for the
    private String description;
    private Date date;
    private Double amount;

    // Specifically for dates
    private Calendar mCalendar;
    private DatePicker mDatePicker;
    private DatePickerDialog dateDialog;
    private String myFormat = "yyyy/MM/dd";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    private onDialogInteractionListener mListener;

    public ExpenseFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ExpenseFragment.
     */
    public static ExpenseFragment newInstance() {
        ExpenseFragment fragment = new ExpenseFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mType = getArguments().getString(ARG_TYPE);
            mExpense = getArguments().getParcelable(ARG_EXPENSE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.dialog_expense, container, false);

        Session session = new Session(getContext());
        userId = session.getUser().id;

        // Editable fields
        expenseDetailDesc = (EditText) rootView.findViewById(R.id.expenseDetailDesc);
        expenseDetailAmount = (EditText) rootView.findViewById(R.id.expenseDetailAmount);

        expenseDetailDate = (EditText) rootView.findViewById(R.id.expenseDetailDate);
        expenseDetailDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateDialog.show();
            }
        });
        expenseDetailDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dateDialog.show();
                return true;
            }
        });
        expenseDetailDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    dateDialog.show();
                }
            }
        });

        expenseDetailCancel = (AppCompatButton) rootView.findViewById(R.id.expenseDetailCancel);
        expenseDetailCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        expenseDetailSubmit = (AppCompatButton) rootView.findViewById(R.id.expenseDetailSubmit);
        expenseDetailSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Check the values and convert to correct format
                String validationError = validate();
                if (validationError.equals("")) {

                    // The Expense parameters have passed through validate()

                    Expense e;
                    if (mType.equals("edit")) {
                        e = new Expense(mExpense.id, description, amount, date, userId);
                        mListener.onUpdate(e);
                    } else {
                        e = new Expense(description, amount, date, userId);
                        mListener.onSubmit(e);
                    }
                    dismiss();
                }
                else {
                    Toast.makeText(getActivity(), validationError, Toast.LENGTH_LONG).show();
                }
            }
        });

        // To show the date in the popup according to the text field
        int mYear, mMonth, mDay;
        mCalendar = Calendar.getInstance();

        // Populate fields if editing expense
        if (mType.equals("edit") && (mExpense!=null)) {
            expenseDetailDesc.setText(mExpense.description);
            expenseDetailAmount.setText(String.format("$%.2f", mExpense.amount));
            expenseDetailDate.setText(mExpense.getFullDateString());
            expenseDetailSubmit.setText("UPDATE");

            mYear = Integer.parseInt(mExpense.getYearString());
            mMonth = mExpense.getMonthInt();
            mDay = mExpense.getDayInMonthInt();
        } else {
            expenseDetailSubmit.setText("CREATE");

            mYear = mCalendar.get(Calendar.YEAR);
            mMonth = mCalendar.get(Calendar.MONTH);
            mDay = mCalendar.get(Calendar.DAY_OF_MONTH);
        }

        // Setup date picker
        dateDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mCalendar.set(Calendar.YEAR, year);
                        mCalendar.set(Calendar.MONTH, monthOfYear);
                        mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        // Setting date in the field
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
                        expenseDetailDate.setText(sdf.format(mCalendar.getTime()));
                    }
                },
                mYear, mMonth, mDay
        );
        return rootView;
    }

    /**
     * Validate the field items and store the values
     * @return
     */
    private String validate() {
        String result = "";

        // Check the description
        description = expenseDetailDesc.getText().toString();
        if ((description == null) || (description.equals(""))) {
            return "Please enter a description";
        }

        // Check the amount - removes the $
        String sAmount = expenseDetailAmount.getText().toString();
        if ((sAmount == null) || (sAmount.equals(""))) {
            return "Please enter an amount";
        }
        // Convert the amount
        try {
            if (sAmount.charAt(0)=='$') { sAmount = sAmount.substring(1); }
            amount = new Double(sAmount);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return "Problem with amount format";
        }

        // Check date
        String sDate = expenseDetailDate.getText().toString();
        if ((sDate == null) || (sDate.equals(""))) {
            return "Please enter a date";
        }
        // Convert the date
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        date = mCalendar.getTime();
        try {
            date = sdf.parse(sDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return "Date should be in this format '" + myFormat +"'";
        }

        return result;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof onDialogInteractionListener) {
            mListener = (onDialogInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement onDialogInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface onDialogInteractionListener {
        void onSubmit(Expense expense);
        void onUpdate(Expense expense);
    }
}
