package com.example.trudy.track_budget_android.utils.api;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by trudy on 2017-07-22.
 * Code from: https://futurestud.io/tutorials/retrofit-2-simple-error-handling
 */

public class ErrorUtils {

    public static ResponseError parseError(Response<?> response) {
        Converter<ResponseBody, ResponseError> converter = RetrofitServiceGenerator.getRetrofit()
                        .responseBodyConverter(ResponseError.class, new Annotation[0]);

        ResponseError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ResponseError();
        }

        return error;
    }
}