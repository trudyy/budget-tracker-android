package com.example.trudy.track_budget_android.models;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by trudy on 2017-07-01.
 */

public class Week {

    public int id;
    public ArrayList<Expense> sun;
    public ArrayList<Expense> mon;
    public ArrayList<Expense> tue;
    public ArrayList<Expense> wed;
    public ArrayList<Expense> thu;
    public ArrayList<Expense> fri;
    public ArrayList<Expense> sat;

    public Week() {
        this.id = -1; // Must set this week in year id to alter Week contents
        this.sun = new ArrayList<>();
        this.mon = new ArrayList<>();
        this.tue = new ArrayList<>();
        this.wed = new ArrayList<>();
        this.thu = new ArrayList<>();
        this.fri = new ArrayList<>();
        this.sat = new ArrayList<>();
    }

    public boolean isEmpty() {
        int count = 0;
        count += sun.size();
        count += mon.size();
        count += tue.size();
        count += wed.size();
        count += thu.size();
        count += fri.size();
        count += sat.size();

        return count == 0;
    }

    public boolean isDayEmpty(int day) {
        switch (day) {
            case 0:
                return sun.size() == 0;
            case 1:
                return mon.size() == 0;
            case 2:
                return tue.size() == 0;
            case 3:
                return wed.size() == 0;
            case 4:
                return thu.size() == 0;
            case 5:
                return fri.size() == 0;
            case 6:
                return sat.size() == 0;
            default:
                return false;
        }
    }


    public void addToDayInWeek(Expense e) {
        // Any item that is added must have the same week in year number
        if ((id != -1) && (id == e.getWeekInYearInt())) {
            int day = e.getDayInWeekInt();
            switch (day) {
                case 0:
                    sun.add(e);
                    break;
                case 1:
                    mon.add(e);
                    break;
                case 2:
                    tue.add(e);
                    break;
                case 3:
                    wed.add(e);
                    break;
                case 4:
                    thu.add(e);
                    break;
                case 5:
                    fri.add(e);
                    break;
                case 6:
                    sat.add(e);
                    break;
            }
        }
    }

    public ArrayList<Expense> getExpenseList() {
        ArrayList<Expense> result = new ArrayList<>();
        for (Expense e: sun) {result.add(e);}
        for (Expense e: mon) {result.add(e);}
        for (Expense e: tue) {result.add(e);}
        for (Expense e: wed) {result.add(e);}
        for (Expense e: thu) {result.add(e);}
        for (Expense e: fri) {result.add(e);}
        for (Expense e: sat) {result.add(e);}
        return result;
    }

    public double[] getTotalForWeek() {
        double[] result = new double[7];
        for (int i=0; i<7; i++) {
            result[i] = getTotalByDay(i);
        }
        return result;
    }

    public double getTotalByDay(int day) {

        ArrayList<Expense> current = new ArrayList<>();
        switch (day) {
            case 0:
                current = sun;
                break;
            case 1:
                current = mon;
                break;
            case 2:
                current = tue;
                break;
            case 3:
                current = wed;
                break;
            case 4:
                current = thu;
                break;
            case 5:
                current = fri;
                break;
            case 6:
                current = sat;
                break;
        }

        double result = 0.0;
        for (Expense e : current) {
            result += e.amount;
        }
        return result;
    }

    public void printouts() {
        Log.e("CURRENT WEEK ", ""+this.id);

        if (sun.size() != 0) {
            for (Expense e : sun) { e.print(); }
        }
        if (mon.size() != 0) {
            for (Expense e : mon) { e.print(); }
        }
        if (tue.size() != 0) {
            for (Expense e : tue) { e.print(); }
        }
        if (wed.size() != 0) {
            for (Expense e : wed) { e.print(); }
        }
        if (thu.size() != 0) {
            for (Expense e : thu) { e.print(); }
        }
        if (fri.size() != 0) {
            for (Expense e : fri) { e.print(); }
        }
        if (sat.size() != 0) {
            for (Expense e : sat) { e.print(); }
        }

    }
}
