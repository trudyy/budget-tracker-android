package com.example.trudy.track_budget_android.ui.detail;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.trudy.track_budget_android.R;
import com.example.trudy.track_budget_android.listeners.OnExpenseListInteractionListener;
import com.example.trudy.track_budget_android.models.Expense;
import com.github.ivbaranov.mli.MaterialLetterIcon;

import java.util.List;

/**
 * Created by trudy on 2017-06-06.
 */

public class MyExpenseListAdapter extends RecyclerView.Adapter<MyExpenseListAdapter.ExpenseHolder> {

    private List<Expense> mValues;
    private final OnExpenseListInteractionListener mListener;


    public MyExpenseListAdapter(List<Expense> items, OnExpenseListInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ExpenseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.content_expense_list_item, parent, false);
        return new ExpenseHolder(view);
    }

    @Override
    public void onBindViewHolder(final ExpenseHolder holder, int position) {
        holder.mItem = mValues.get(position);

        String firstLetter = String.valueOf(holder.mItem.description.charAt(0));
        holder.mIcon.setLetter(firstLetter);

        // Get array of colours from:
        // https://stackoverflow.com/questions/40284802/how-to-output-a-random-color-from-a-set-of-selected-colors-in-java-android
        int[] androidColors = holder.mView.getResources().getIntArray(R.array.androidcolors);
        int randomAndroidColor = androidColors[(((int) firstLetter.charAt(0)) % androidColors.length)];
        holder.mIcon.setShapeColor(randomAndroidColor);

        holder.mDescription.setText(String.valueOf(holder.mItem.description));
        holder.mDate.setText(
//                String.valueOf(holder.mItem.getMonthDayString()
//                + "\n"
//                + holder.mItem.getWeekDayString()
//                + "\n"
//                + holder.mItem.getYearString()
//                + "\n"
//                +
                        holder.mItem.getFullDateString());
        holder.mAmount.setText(formatCurrency(holder.mItem.amount));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClickExpenseItem(holder.mItem);
            }
        });

        holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mListener.onDeleteExpenseItem(holder.mItem);
                return false;
            }
        });
    }

    // Update the whole list
    public void setInfo(List<Expense> list){
        mValues = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if ((mValues != null) && (!mValues.isEmpty())) {
            return mValues.size();
        } else {
            return 0;
        }
    }

    private String formatCurrency(double amount) {
        String sign = amount < 10 ? "$ 0": "$ ";
        return sign + String.format("%.2f", amount);
    }

    /**
     * Content holding the expense object
     */
    public class ExpenseHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final MaterialLetterIcon mIcon;
        public final TextView mAmount;
        public final TextView mDescription;
        public final TextView mDate;
        public Expense mItem;

        public ExpenseHolder(View view) {
            super(view);
            mView = view;
            mIcon = (MaterialLetterIcon) view.findViewById(R.id.listItemLetter);
            mAmount = (TextView) view.findViewById(R.id.listItemAmount);
            mDescription = (TextView) view.findViewById(R.id.listItemDescription);
            mDate = (TextView) view.findViewById(R.id.listItemDate);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mDescription.getText() + "'";
        }
    }
}