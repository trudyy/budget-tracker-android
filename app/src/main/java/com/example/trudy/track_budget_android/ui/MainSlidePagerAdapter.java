package com.example.trudy.track_budget_android.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.example.trudy.track_budget_android.models.Expense;
import com.example.trudy.track_budget_android.models.Week;
import com.example.trudy.track_budget_android.ui.MainActivity;
import com.example.trudy.track_budget_android.ui.dashboard.DashboardFragment;
import com.example.trudy.track_budget_android.ui.detail.DetailsFragment;
import com.example.trudy.track_budget_android.ui.graph.GraphFragment;
import com.example.trudy.track_budget_android.utils.ExpenseUtils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by trudy on 2017-08-07.
 * Reference: https://developer.android.com/training/animation/screen-slide.html#pagetransformer
 *
 * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
 * sequence.
 */
public class MainSlidePagerAdapter extends FragmentStatePagerAdapter {

    private String mType;

    // Expenses ordered into weeks
    private HashMap<Integer, Week> orderedIntoWeeks;

    // Constructor
    public MainSlidePagerAdapter(String type, FragmentManager fm, ArrayList<Expense> all_expenses) {
        super(fm);
        mType = type;
        orderedIntoWeeks = (new ExpenseUtils()).groupExpenseByWeek(all_expenses);
    }

    // Instantiate each page
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        return super.instantiateItem(container, position);
    }

    // Get the fragment
    @Override
    public Fragment getItem(int position) {
        Bundle arguments = new Bundle();

        ArrayList<Expense> mListExpenses = new ArrayList<>();
        double[] mTotal = new double[7];
        if ((orderedIntoWeeks != null) && (orderedIntoWeeks.get(position) != null)) {
            mListExpenses = orderedIntoWeeks.get(position).getExpenseList();
            mTotal = orderedIntoWeeks.get(position).getTotalForWeek();
        }
        arguments.putParcelableArrayList(MainActivity.ARG_EXPENSES, mListExpenses);
        arguments.putDoubleArray(MainActivity.ARG_TOTALPERDAY, mTotal);
        arguments.putInt(MainActivity.ARG_WEEKINYEAR, position);

        switch (mType) {
            case "dashboard":
                return DashboardFragment.newInstance(arguments);
            case "graph":
                return GraphFragment.newInstance(arguments);
            case "details":
                return DetailsFragment.newInstance(arguments);
            default:
                return DashboardFragment.newInstance(arguments);
        }
    }

    @Override
    public int getCount() {
        return 52;
    }
}