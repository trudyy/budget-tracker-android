package com.example.trudy.track_budget_android.listeners;

import com.example.trudy.track_budget_android.models.Expense;

/**
 * Created by trudy on 2017-06-06.
 */

public interface OnExpenseListInteractionListener {
    void onDeleteExpenseItem(Expense expense);
    void onClickExpenseItem(Expense expense);
    void onAddExpenseItem();
}
