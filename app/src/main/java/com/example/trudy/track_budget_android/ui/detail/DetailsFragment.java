package com.example.trudy.track_budget_android.ui.detail;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.trudy.track_budget_android.R;
import com.example.trudy.track_budget_android.listeners.OnExpenseListInteractionListener;
import com.example.trudy.track_budget_android.models.Expense;
import com.example.trudy.track_budget_android.ui.MainActivity;

import java.util.ArrayList;

public class DetailsFragment extends Fragment {

    private View rootView;
    private Context context;
    private RecyclerView recyclerView;

    private int mColumnCount = 1;
    private int weekInYear;

    // List Adapter
    private MyExpenseListAdapter mExpenseListAdapter;
    private ArrayList<Expense> mListExpenses = new ArrayList<>();

    // Listener
    private OnExpenseListInteractionListener mListener;

    public DetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GraphFragment.
     */
    public static DetailsFragment newInstance(Bundle bundle) {
        DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Passed by MainSlidePagerAdapter
        if (getArguments() != null) {
            weekInYear = getArguments().getInt(MainActivity.ARG_WEEKINYEAR);
            mListExpenses = getArguments().getParcelableArrayList(MainActivity.ARG_EXPENSES);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_details, container, false);

        // Set the adapter
        recyclerView = (RecyclerView) rootView.findViewById(R.id.expense_list);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        mExpenseListAdapter = new MyExpenseListAdapter(new ArrayList<Expense>(), mListener);
        recyclerView.setAdapter(mExpenseListAdapter);

        // Show the list according to the week in year
        updateListView();

        return rootView;
    }

    public void updateListView() {

        // Setup the recycler view
        mExpenseListAdapter.setInfo(mListExpenses);
        mExpenseListAdapter.notifyDataSetChanged();
        updateEmptyView();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnExpenseListInteractionListener) {
            mListener = (OnExpenseListInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnExpenseListInteractionListener");
        }
    }

    private void updateEmptyView() {
        LinearLayout emptyView = (LinearLayout) rootView.findViewById(R.id.empty_expense_list);
        RecyclerView expenseList = (RecyclerView) rootView.findViewById(R.id.expense_list);

        if (mExpenseListAdapter.getItemCount() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            expenseList.setVisibility(View.INVISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
            expenseList.setVisibility(View.VISIBLE);
        }
    }

}
